const Announcement = require("../../models/Announcement");

module.exports = async (req, res, next) => {
    const { announcement_id } = req.params;

    const announcement = await Announcement.findByPk(announcement_id);

    if (!announcement) {
        return res.status(202).json({ error: true, message: "Usuário não encontrado." });
    }

    next();
}