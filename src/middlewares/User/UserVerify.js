const User = require("../../models/User");

module.exports = async (req, res, next) => {
    const { user_id } = req.params;

    const user = await User.findByPk(user_id);

    if (!user) {
        return res.status(202).json({ error: true, message: "Usuário não encontrado." });
    }

    next();
}