const jwt = require("jsonwebtoken");

module.exports = {
    async verify(req, res, next) {
        const token = req.headers['x-access-token'];

        if (!token) return res.status(401).json({ error: true, message: 'Token não foi informado.' });

        jwt.verify(token, process.env.APP_KEY, function(error, decoded) {
            if (error) return res.status(401).json({ error: true, message: 'Token expirado.' });
            
            req.access_token = token;

            next();
        });
    }
};