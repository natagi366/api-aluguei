const Sequelize = require("sequelize");
const dbConfig = require("../config/database");

const connection = new Sequelize(dbConfig);

const User = require("../models/User");
const Announcement = require("../models/Announcement");

User.init(connection);
Announcement.init(connection);

Announcement.associate(connection.models);

module.exports = connection;