const express = require("express");
const routes = require("./routes");
const ExpressMiddleware = require("./middlewares/ExpressMiddleware");

require('dotenv/config');
require('./database');

const app = express();

app.use(express.json());
app.use(routes);
app.use(ExpressMiddleware.error);

console.log('Server up! Port 3000');
app.listen(3000);