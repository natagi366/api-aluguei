const { Model, DataTypes } = require("sequelize");

class Announcement extends Model {
    static init(sequelize) {
        super.init({
            announcement_id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            name: DataTypes.STRING,
            description: DataTypes.STRING,
            user_id: DataTypes.INTEGER,
            value: DataTypes.FLOAT,
            value_type: DataTypes.INTEGER
        }, { sequelize });
    }

    static associate(models) {
        this.belongsTo(models.User, { foreignKey: "user_id", as: "user"});
    }
}

module.exports = Announcement;