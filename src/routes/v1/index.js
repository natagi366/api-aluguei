const express = require("express");
const routes = express.Router();

// Middlewares
const JwtMiddleware = require("../../middlewares/JwtMiddleware");

// Requests
const UserVerify = require("../../middlewares/User/UserVerify");
const AnnouncementVerify = require("../../middlewares/Announcement/AnnouncementVerify");

// Controllers
const AuthController = require("../../controllers/AuthController");
const UserController = require("../../controllers/UserController");
const AnnouncementController = require("../../controllers/AnnouncementController");

// Rotas sem autenticação
routes.post(`/auth/token`, AuthController.authToken);
routes.post("/users", UserController.create);

// Rotas com autenticação
routes.use(JwtMiddleware.verify);

routes.get("/users/:user_id", UserVerify, UserController.show);
routes.delete("/users/:user_id", UserVerify, UserController.delete);
routes.put("/users/:user_id", UserVerify, UserController.update);

routes.get("/users/:user_id/announcements", UserVerify, AnnouncementController.index);
routes.get("/announcements/:announcement_id", AnnouncementController.show);
routes.post("/users/:user_id/announcements", UserVerify, AnnouncementController.create);
routes.delete("/announcements/:announcement_id", AnnouncementVerify, AnnouncementController.delete);
routes.put("/announcements/:announcement_id", AnnouncementVerify, AnnouncementController.update);

module.exports = routes;