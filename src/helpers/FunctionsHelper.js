const path = require('path');

module.exports = {
    resolve: (path) => {
        return path.resolve(__dirname, path);
    }
};