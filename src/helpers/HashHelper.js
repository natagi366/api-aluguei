const bcrypt = require('bcrypt');

module.exports = {
    generate: async (plainText, saltRounds = 5) => {
        return new Promise((res) => {
            bcrypt.genSalt(saltRounds, (err, salt) => {
                bcrypt.hash(plainText, salt, (err, hash) => {
                    res(hash);
                });
            });
        });
    },
    compare: (plainText, hash) => {
        return new Promise((res) => {
            bcrypt.compare(plainText, hash, (err, result) => {
                res(result);
            });
        });
    }
};