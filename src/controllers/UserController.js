const User = require("../models/User");
const HashHelper = require("../helpers/HashHelper");

module.exports = {
    async create(req, res) {
        try {
            const { name, surname, cpf_cnpj, phone, email, password } = req.body;
    
            const user = await User.findOne({ where: { email } });
    
            if (user) {
                return res.status(422).json({ error: true, message: "Usuário já cadastrado para este e-mail." });
            }
    
            const passwordHash = await HashHelper.generate(password);
    
            const newUser = await User.create({ name, surname, cpf_cnpj, phone, email, password: passwordHash });
    
            return res.json({ error: false, data: newUser });
        } catch (error) {
            return res.status(500).json({ error: true, message: error });
        }
    },

    async show(req, res) {
        const { user_id } = req.params;

        const user = await User.findByPk(user_id);

        return res.json({ error: false, data: user });
    },

    async delete(req, res) {
        const { user_id } = req.params;

        await User.destroy({ where: { user_id }});

        return res.json({ error: false, message: "Usuário deletado." });
    },

    async update(req, res) {
        const { user_id } = req.params;
        const { name, surname, cpf_cnpj, phone, email, password } = req.body;

        await User.update({ name, surname, cpf_cnpj, phone, email, password }, { where: { user_id } });

        return res.json({ error: false, message: "Usuário atualizado." });
    }
};