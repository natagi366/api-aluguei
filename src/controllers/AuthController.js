const jwt = require("jsonwebtoken");
const HashHelper = require("../helpers/HashHelper");
const User = require("../models/User");

module.exports = {
    async authToken(req, res) {
        const { email, password } = req.body;

        const user = await User.findOne({ where: { email } });

        if (!user) {
            return res.json({
                error: true,
                message: "Usuário não encontrado."
            });
        }

        if (!await HashHelper.compare(password, user.password)) {
            return res.json({
                error: true,
                message: "Senha incorreta."
            });
        }

        const expires_in = parseInt(process.env.TOKEN_EXPIRES);

        const access_token = jwt.sign({ user_id: user.user_id }, process.env.APP_KEY, {
            expiresIn: expires_in
        });

        return res.json({
            expires_in,
            access_token
        });
    }
};