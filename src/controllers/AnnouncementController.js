const Announcement = require("../models/Announcement");

module.exports = {
    async index(req, res) {
        const { user_id } = req.params;

        const announcements = await Announcement.findAll({ where: { user_id }});

        return res.json({ error: false, data: announcements });
    },

    async create(req, res) {
        const { user_id } = req.params;
        const { name, description, value, value_type } = req.body;

        const announcement = await Announcement.create({ name, description, user_id, value, value_type });

        return res.json({ error: false, data: announcement });
    },

    async show(req, res) {
        const { announcement_id } = req.params;

        const announcement = await Announcement.findByPk(announcement_id);

        return res.json({ error: false, data: announcement });
    },

    async delete(req, res) {
        const { announcement_id } = req.params;

        await Announcement.destroy({ where: { announcement_id } });

        return res.json({ error: false, message: "Anúncio deletado." });
    },

    async update(req, res) {
        const { announcement_id } = req.params;
        const { name, description, user_id, value, value_type } = req.body;

        await Announcement.update({ name, description, user_id, value, value_type }, { where: { announcement_id } });

        return res.json({ error: false, message: "Anúncio atualizado." });
    }
};